#include <libpq-fe.h>
#include <iostream>
#include <cstring>
#include <iomanip>

using namespace std;

int main()
{
  int code_retour;
  code_retour = 0;
  char connexion_serveur[] = "host=postgresql.bts-malraux72.net port=5432 dbname=a.rondeau user=a.rondeau password=P@ssword";
  PGPing code_retour_ping;
  code_retour_ping = PQping(connexion_serveur);
  int nbColonnes, nbLignes, taille;
  PGconn *connexion_bdd;

  if(code_retour_ping == PQPING_OK)  //Vérification de l'accessibilité du serveur
  {
    connexion_bdd = PQconnectdb(connexion_serveur);

    if(PQstatus(connexion_bdd) == CONNECTION_OK)  // Vérification de la connectivité a la base de donnée
    {
      cout << " La connexion au serveur de base de données 'postgresql.bts-malraux72.net' a été établie avec les paramètres suivants : " << endl;
      cout << " utilisateur : " << PQuser(connexion_bdd) << endl; //recupère l'utilisateur qui s'est connecté
      unsigned int nb_char;
      nb_char	= sizeof PQpass(connexion_bdd); // détermination de la taille du mot de passe
      cout << " mot de passe : ";

      //affichage du mot de passe en "*" en fonction du nombre de caractère de ce dernier
      for(unsigned int i = 0; i < nb_char; ++i)
      {
        cout << "*";
      }

      //Affichage des informations en relation avec la connexion effectuée
      cout << endl;
      cout << " base de données : " << PQdb(connexion_bdd) << endl;
      cout << " port TCP : " << PQport(connexion_bdd) << endl;
      cout << " chiffrement SSL : " << PQgetssl(connexion_bdd) << endl;
      cout << " encodage : " << PQclientEncoding(connexion_bdd) << endl;
      cout << " version du serveur : " << PQserverVersion(connexion_bdd) << endl;
      const char requete_styley[] = "SET schema 'si6'; SELECT * FROM \"Animal\" INNER JOIN \"Race\" ON \"Animal\".race_id = \"Race\".id WHERE sexe = 'Femelle' AND \"Race\".nom = 'Singapura';";
      PGresult *requete = PQexec(connexion_bdd, requete_styley);
      ExecStatusType etat_r = PQresultStatus(requete);

      //Gestion des erreurs possibles (Aidée par Samuel Covin)
      if(etat_r == PGRES_EMPTY_QUERY)
      {
        cerr << "La chaîne envoyée était vide.";
      }
      else if(etat_r == PGRES_COMMAND_OK)
      {
        cerr << "Fin avec succès d'une commande ne renvoyant aucune donnée.";
      }
      else if(etat_r == PGRES_TUPLES_OK)
      {
        nbColonnes = PQnfields(requete);
        int nbEnregistrements = PQntuples(requete);
        int tiret = 0;
        nbLignes = PQntuples(requete);

        for(int ligne = 0; ligne < nbLignes; ligne++)
        {
          for(int colonnes = 0; colonnes < nbColonnes; colonnes++)
          {
            int titre = strlen(PQfname(requete, colonnes));

            if(titre > taille)
            {
              taille = titre;
            }
          }
        }

        for(int i = 0; i < nbColonnes; i++)
        {
          setiosflags(ios::left);
          cout << left << setw(taille) << PQfname(requete, i) << "|";
        }

        for(int j = 0; j < nbLignes; j++)
        {
          for(int k = 0; k < nbColonnes; k++)
          {
            cout << setw(taille) << PQgetvalue(requete, j, k) << "|";
          }
        }

        cout << endl << "L'execution de la requete SQL a retourne " << nbEnregistrements << " enregistrements." << endl;
        PQclear(requete);
      }
      else if(etat_r == PGRES_COPY_OUT)
      {
        cerr << "Debut de l'envoie (à partir du serveur) d'un flux de données.";
      }
      else if(etat_r == PGRES_COPY_IN)
      {
        cerr << "Début de la réception (sur le serveur) d'un flux de données.";
      }
      else if(etat_r == PGRES_BAD_RESPONSE)
      {
        cerr << "La reponse du serveur comprise.";
      }
      else if(etat_r == PGRES_NONFATAL_ERROR)
      {
        cerr << "Une erreur non fatale (une note ou un avertissement) est survenue.";
      }
      else if(etat_r  == PGRES_FATAL_ERROR)
      {
        cerr << "Une erreur fatale est survenue.";
      }
      else
      {
        cerr << "Erreur de connexion" << endl;
      }
    }
    else if(PQstatus(connexion_bdd) != CONNECTION_OK)
    {
      cerr << "La connexion au serveur ne se fait pas. Veuillez contacter votre administateur réseau" << endl;
      code_retour = 1;
    }
    else
    {
      cerr << "Le serveur ne semble pas etre accessible. Veuillez contacter votre administrateur systeme" << endl;
      code_retour = 1;
    }

    PQfinish(connexion_bdd);
  }

  return code_retour;
}
